#include "serialport.h"

SerialPort::SerialPort(QObject *parent):QObject(parent)
{

}

QString SerialPort::readPort()
{
    int value= QRandomGenerator::global()->bounded(0,101);
    qDebug() << "Random Value: " << QString::number(value);
    return QString::number(value);
}

