#pragma once

#include <QDebug>


#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QRandomGenerator> // Just for testing

#include <QObject>

class SerialPort : public QObject
{
    Q_OBJECT

public:
    explicit SerialPort (QObject *parent = nullptr);
    QString readPort ();


private:

};
