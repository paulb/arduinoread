#pragma once

#include <QDebug>


#include <QObject>


class Write2TextArea : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString displayText READ displayText WRITE setDisplayText NOTIFY changedDisplayText)

public:
    explicit Write2TextArea (QObject *parent = nullptr);
    void setDisplayText(const QString &displayText);
    Q_SIGNAL void changedDisplayText();
    QString displayText() const;

  //  void writeAreaText(QObject object, QString text);

public slots:
    QString readAreaText(QString text);

private:
    QString m_displayText = "Hello World!";
    QString m_contentText;
};
