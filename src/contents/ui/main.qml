// Includes relevant modules used by the QML
import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.20 as Kirigami

import org.kde.arduinoread 1.0

// Provides basic features needed for all kirigami applications
Kirigami.ApplicationWindow {
    // Unique identifier to reference this object
    id: root

    // Window title
    // i18nc() makes a string translatable
    // and provides additional context for the translators
    title: i18nc("@title:window", "Arduino Data")

    // Set the first page that will be loaded when the app opens
    // This can also be set to an id of a Kirigami.Page
    //pageStack.initialPage: Kirigami.Page {
        Controls.TextArea {
            // Fill parent object
            id: showdata
            anchors.fill: parent
            placeholderText: i18n("Data will appear here\n")
            text: "Hello today!"
            onTextChanged: {
            // Call C++ function to read text
                var text = Write2TextArea.readAreaText(showdata.text)
            }

            Column {
                /*
                TextEdit {
                    objectName: "first"
                    textMargin: 10
                    text: "Hello!"
                }
                TextEdit {
                    text: Write2TextArea.displayText
                }
                */
            }
        }

    //}
}
