#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QtQml>
#include <QUrl>
#include <KLocalizedContext>
#include <KLocalizedString>

#include <QDebug>

#include "write2TextArea.h"
#include "serialport.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    KLocalizedString::setApplicationDomain("ArduinoData");

    Write2TextArea w2ta;
    SerialPort sp;

    qmlRegisterSingletonInstance<Write2TextArea>("org.kde.arduinoread", 1, 0, "Write2TextArea", &w2ta);

    SerialPort arduino;
    qmlRegisterSingletonInstance<SerialPort>("org.kde.arduinoread", 1, 0, "SerialPort", &arduino);

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    QQmlComponent component(&engine);

    QQuickItem* textArea = engine.rootObjects().at(0)->findChild<QQuickItem*>("showdata");
    qDebug()<<"I get this: "<<textArea->property("text");

    //textArea->setProperty("text", "Hello World!");
    /*QObject *object = component.create();

    QObject *textArea = object->findChild<QObject*>("showdata");
    if (textArea) textArea->setProperty("text", "Hellllooooooo!!!!");
    */

    //w2ta.writeAreaText(*object, sp.readPort());


    return app.exec();
}
