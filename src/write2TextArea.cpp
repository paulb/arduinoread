#include "write2TextArea.h"

Write2TextArea::Write2TextArea(QObject *parent):QObject(parent)
{


}

void Write2TextArea::setDisplayText(const QString &displayText)
{
    m_displayText = displayText;
}

QString Write2TextArea::displayText() const
{
    return m_displayText;
}


QString Write2TextArea::readAreaText(QString text)
{
    qDebug()<<"Text Area says: " << text;
    return text;
}

/*

void Write2TextArea::writeAreaText(QObject object, QString text)
{
    QObject *textArea = object->findChild<QObject*>("showdata");
    if (textArea) textArea->setProperty("text", text);
}
*/
